<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function home(Request $request): Response
    {
        return $this->render('pages/home.html.twig');
    }

    #[Route('/generate-password', name: 'app_generate')]
    public function generate(Request $request): Response
    {
        $characters = range('a','z');
        $longueur = $request->query->getInt('longueur');
        $majuscule = $request->query->getBoolean('majuscule');
        $nombre = $request->query->getBoolean('nombre');
        $char_spec = $request->query->getBoolean('char_spec');

        if($majuscule) {
            $characters = array_merge($characters, range('A', 'Z'));
        }

        if($nombre) {
            $characters = array_merge($characters, range(0,9));
        }

        if($char_spec) {
            $characters = array_merge($characters, ['!', '#', '$', '%', '&', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~']);
        }

        $password = '';


        for ($i=0; $i < $longueur; $i++) {
           $password = $password . $characters[mt_rand(0, count($characters) -1)];
        }

        return $this->render('pages/password.html.twig', [
            'password' => $password
        ]);
    }
}
